# translation of kio_nfs.po to Uzbek
# Copyright (C) 2003, 2004 Free Software Foundation, Inc.
# Mashrab Kuvatov <kmashrab@uni-bremen.de>, 2003, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 00:46+0000\n"
"PO-Revision-Date: 2004-07-15 01:01+0200\n"
"Last-Translator: Mashrab Kuvatov <kmashrab@uni-bremen.de>\n"
"Language-Team: Uzbek <floss-uz-l10n@googlegroups.com>\n"
"Language: uz@cyrillic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.3\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: kio_nfs.cpp:152
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr ""

#: kio_nfs.cpp:322
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr ""

#: kio_nfs.cpp:352
#, kde-format
msgid "Failed to initialise protocol"
msgstr ""

#: kio_nfs.cpp:820
#, kde-format
msgid "RPC error %1, %2"
msgstr ""

#: kio_nfs.cpp:869
#, kde-format
msgid "Filename too long"
msgstr "Файлнинг номи жуда узун"

#: kio_nfs.cpp:876
#, kde-format
msgid "Disk quota exceeded"
msgstr "Диск квота чегарасидан ўтиб кетди"

#: kio_nfs.cpp:882
#, kde-format
msgid "NFS error %1, %2"
msgstr ""

#: nfsv2.cpp:391 nfsv2.cpp:442 nfsv3.cpp:429 nfsv3.cpp:573
#, kde-format
msgid "Unknown target"
msgstr ""

#~ msgid "No space left on device"
#~ msgstr "Ускунада жой қолмади"

#~ msgid "Read only file system"
#~ msgstr "Фақат ўқиб бўладиган файл тизими"

#~ msgid "An RPC error occurred."
#~ msgstr "RPC хатоси рўй берди."
