# kio_man Bahasa Melayu (Malay) (ms)
# Copyright (C) 2008, 2009 K Desktop Environment
#
# Muhammad Najmi Ahmad Zabidi <md_najmi@yahoo.com>, 2003.
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_man\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 00:46+0000\n"
"PO-Revision-Date: 2009-03-12 23:57+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=1;\n"

#: kio_man.cpp:456
#, fuzzy, kde-kuit-format
#| msgid ""
#| "No man page matching to %1 found.<br /><br />Check that you have not "
#| "mistyped the name of the page that you want.<br />Check that you have "
#| "typed the name using the correct upper and lower case characters.<br />If "
#| "everything looks correct, then you may need to improve the search path "
#| "for man pages; either using the environment variable MANPATH or using a "
#| "matching file in the /etc directory."
msgctxt "@info"
msgid ""
"No man page matching <resource>%1</resource> could be found.<nl/><nl/>Check "
"that you have not mistyped the name of the page, and note that man page "
"names are case sensitive.<nl/><nl/>If the name is correct, then you may need "
"to extend the search path for man pages, either using the <envar>MANPATH</"
"envar> environment variable or a configuration file in the <filename>/etc</"
"filename> directory."
msgstr ""
"Tiada halaman man sepadan dengan %1 dijumpai.<br /><br />Pastikan yang anda "
"tidak salah taip nama halaman yang anda mahu.<br />Pastikan yang anda mesti "
"menaip nama dengan aksara besar dan kecil yang betul.<br />Jika semuanya "
"kelihatan betul, mungkin anda perlu untuk memperbaiki laluan carian untuk "
"halaman man, sama ada dengan pembolehubah persekitaran MANPATH atau fail "
"yang sepadan dalam direktori /etc ."

#: kio_man.cpp:570
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The specified man page references another page <filename>%1</filename>,<nl/"
">but the referenced page <filename>%2</filename> could not be found."
msgstr ""

#: kio_man.cpp:587
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be read."
msgstr ""

#: kio_man.cpp:596
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be converted."
msgstr ""

#: kio_man.cpp:658
#, fuzzy, kde-format
#| msgid "<h1>KDE Man Viewer Error</h1>"
msgid "Manual Page Viewer Error"
msgstr "<h1>Ralat Pelihat Man KDE</h1>"

#: kio_man.cpp:671
#, fuzzy, kde-format
#| msgid "There is more than one matching man page."
msgid "There is more than one matching man page:"
msgstr "Ada lebih dari satu padanan laman manual."

#: kio_man.cpp:671
#, kde-format
msgid "Multiple Manual Pages"
msgstr ""

#: kio_man.cpp:684
#, kde-format
msgid ""
"Note: if you read a man page in your language, be aware it can contain some "
"mistakes or be obsolete. In case of doubt, you should have a look at the "
"English version."
msgstr ""
"Nota: jika anda membaca halaman man dalam bahasa anda, ambil perhatian ia "
"mungkin mengandungi beberapa kesalahan atau telah lapuk. Jika mempunyai "
"persoalan, anda perlu melihat dalam versi Bahasa Inggeris."

#: kio_man.cpp:752
#, kde-format
msgid "Header Files"
msgstr ""

#: kio_man.cpp:754
#, kde-format
msgid "Header Files (POSIX)"
msgstr ""

#: kio_man.cpp:756
#, kde-format
msgid "User Commands"
msgstr "Arahan Pengguna"

#: kio_man.cpp:758
#, fuzzy, kde-format
#| msgid "User Commands"
msgid "User Commands (POSIX)"
msgstr "Arahan Pengguna"

#: kio_man.cpp:760
#, kde-format
msgid "System Calls"
msgstr "Panggilan Sistem"

#: kio_man.cpp:762
#, kde-format
msgid "Subroutines"
msgstr "Subrutin"

#: kio_man.cpp:765
#, kde-format
msgid "Perl Modules"
msgstr "Modul Perl"

#: kio_man.cpp:767
#, kde-format
msgid "Network Functions"
msgstr "Fungsi Rangkaian"

#: kio_man.cpp:769
#, kde-format
msgid "Devices"
msgstr "Peranti"

#: kio_man.cpp:771
#, kde-format
msgid "File Formats"
msgstr "Format Fail"

#: kio_man.cpp:773
#, kde-format
msgid "Games"
msgstr "Permainan"

#: kio_man.cpp:775
#, kde-format
msgid "Miscellaneous"
msgstr "Pelbagai"

#: kio_man.cpp:777
#, kde-format
msgid "System Administration"
msgstr "Pentadbiran Sistem"

#: kio_man.cpp:779
#, kde-format
msgid "Kernel"
msgstr "Kernel"

#: kio_man.cpp:781
#, kde-format
msgid "Local Documentation"
msgstr "Dokumentasi Setempat"

#: kio_man.cpp:784
#, kde-format
msgid "New"
msgstr "Baru"

#: kio_man.cpp:811
#, fuzzy, kde-format
#| msgid "UNIX Manual Index"
msgid "Main Manual Page Index"
msgstr "Indeks Manual UNIX"

#: kio_man.cpp:836
#, kde-format
msgid "Section %1"
msgstr "Seksyen %1"

#: kio_man.cpp:1085
#, fuzzy, kde-format
#| msgid "Index for Section %1: %2"
msgid "Index for section %1: %2"
msgstr "Indeks untuk Seksyen %1: %2"

#: kio_man.cpp:1085
#, fuzzy, kde-format
#| msgid "UNIX Manual Index"
msgid "Manual Page Index"
msgstr "Indeks Manual UNIX"

#: kio_man.cpp:1097
#, kde-format
msgid "Generating Index"
msgstr "Mejana Indeks"

#: kio_man.cpp:1351
#, fuzzy, kde-kuit-format
#| msgid ""
#| "Could not find the sgml2roff program on your system. Please install it, "
#| "if necessary, and extend the search path by adjusting the environment "
#| "variable PATH before starting KDE."
msgctxt "@info"
msgid ""
"Could not find the <command>%1</command> program on your system. Please "
"install it if necessary, and ensure that it can be found using the "
"environment variable <envar>PATH</envar>."
msgstr ""
"Tidak dapat cari program sgml2roff pada sistem anda, Sila pasang jika perlu, "
"dan lanjutkan laluan carian dengan mengubah pembolehubah persekitaran PATH "
"sebelum memulakan KDE."

#~ msgid "Open of %1 failed."
#~ msgstr "Buka %1 gagal."

#~ msgid "Man output"
#~ msgstr "Output Man"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Muhammad Najmi Ahmad Zabidi, Sharuzzaman Ahmat Raslan"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "md_najmi@yahoo.com, sharuzzaman@myrealbox.com"

#~ msgid "KMan"
#~ msgstr "KMan"
